call plug#begin('~/.vim/plugged')
  Plug 'vim-airline/vim-airline'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'pangloss/vim-javascript'
  Plug 'restore_view.vim'
call plug#end()

" neovim defaults
filetype plugin indent on
syntax enable
set autoindent
set laststatus=2

" theme
set background=light

" tab width & force tab to spaces
set tabstop=2
set shiftwidth=2
set shiftround
set softtabstop=2
set expandtab

" reduce that braces yo
set foldmethod=syntax

" need for speed
set lazyredraw
set ttyfast

" combined number and relativenumber
set nu
set rnu

" all tmp in same place
set swapfile
set dir=/tmp
set backupdir=/tmp

set wildignore+=*/tmp/*,*.swp,*/.git/*

" open split in below and right instead of top and left
set splitbelow
set splitright

" that 80 col marker
highlight ColorColumn ctermbg=01
call matchadd('ColorColumn', '\%81v', 100)

if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  let g:ctrlp_user_command = 'ag -Q -l --nocolor --hidden --ignore ".git" -g "" %s'
  let g:ctrlp_use_caching = 0
endif

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" remove trailingwhitespace
function! <SID>TrimWhitespaces()
  let cursor = getpos('.')
  %s/\s\+$//e
  call setpos('.', cursor)
endfun

augroup trim_whitespace
  au!
  au BufWritePre * :call <SID>TrimWhitespaces()
augroup END

" don't open fold below unmatched fold
augroup manual_fold
  au!
  au InsertEnter * setlocal foldmethod=manual
  au InsertLeave,WinLeave * setlocal foldmethod=syntax
augroup END

" some highlight
augroup hilite
  au!
  au BufRead,BufNewFile  /etc/nginx/*  set syntax=nginx
  au BufRead,BufNewFile  /etc/php*/*   set syntax=dosini
augroup END
