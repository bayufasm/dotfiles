#!env bash

# Go to current script location.
cd "$(dirname $0)";

# Rsync it to homedir."
rsync --exclude .git/ --exclude source.sh -avh --no-perms . ~;

# If vim-plug exist, don't download it again
if [[ ! -f ~/.vim/autoload/plug.vim ]]; then
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

vim +PlugUpdate! +qall

source ~/.profile
