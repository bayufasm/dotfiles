# Set various history variable
# Unlimited history! Yay.
export HISTCONTROL="ignoreboth"
export HISTSIZE=
export HISTFILESIZE=
export HISTIGNORE="bg:fg:history*"
export HISTTIMEFORMAT="%F %T "
