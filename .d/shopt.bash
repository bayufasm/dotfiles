# Enable bash feature.
# Why one would develop feature if it's unused?

option=(
# Don't overwrite history, append it.
histappend

# Save multiline command into one.
cmdhist

# Writing dir to shell automatically change dir.
autocd

# Attempt to fix cd typos.
cdspell

# Various globbing character.
extglob

# Expand "**".
globstar

# Check dimension and update LINES and COLS if necessary.
checkwinsize
)

# No need for loop!
shopt -s ${option[@]} 2> /dev/null
